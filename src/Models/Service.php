<?php

namespace Minioak\Whistl\Models;

class Service
{
    public $name;

    public $description;

    public $providerName;

    public $preference;
}