<?php

namespace Minioak\Whistl\Models;

use JMS\Serializer\Annotation as Serializer;
use Minioak\Whistl\Models\ServicePreference;

class ServicePreferenceList
{
    public $servicePreferences = [];
}