<?php

namespace Minioak\Whistl\Models;

class ServicePreference
{
    public $id;

    public $name;

    public $preferences = [];
}