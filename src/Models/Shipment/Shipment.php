<?php

namespace Minioak\Whistl\Models\Shipment;

class Shipment
{
    public $account;

    public $id;

    public $collectionDetails;

    public $collectionAddress;

    public $deliveryAddress;

    public $reference1;

    public $reference2;

    public $contentsDescription;

    public $packages = [];

    public $enhancements = [];

    public $customsDeclarationInfo;

    public $shipmentTags = [];

    public $serviceInfo;

    public $shippingInfo;

    public $department;

    public function label()
    {
        return $this->packages[0]->packageShippingInfo->labels[0]->image;
    }

    public function trackingNumber()
    {
        return $this->shippingInfo->trackingNumber;
    }

    public function __construct($deliveryData, $senderData)
    {
        $deliveryAddress = new Address();
        $deliveryAddress->contactName = substr($deliveryData['name'], 0, 32);
        $deliveryAddress->address1 = $deliveryData['addressLine1'];
        $deliveryAddress->address2 = $deliveryData['addressLine2'];
        $deliveryAddress->city = $deliveryData['city'];
        $deliveryAddress->postcode = $deliveryData['postcode'];
        $deliveryAddress->email = $deliveryData['email'];
        $deliveryAddress->country = $deliveryData['country'];
        $deliveryAddress->phone = $deliveryData['phone'];

        $senderAddress = new Address();
        $senderAddress->companyName = substr($senderData['name'], 0, 32);
        $senderAddress->contactName = $senderData['contactName'];
        $senderAddress->address1 = $senderData['addressLine1'];
        $senderAddress->address2 = $senderData['addressLine2'];
        $senderAddress->city = $senderData['city'];
        $senderAddress->postcode = $senderData['postcode'];
        $senderAddress->email = $senderData['email'];
        $senderAddress->country = $senderData['country'];

        $package = new Package();
        $package->weight = $deliveryData['weight'];
        $package->type = $deliveryData['packageType'];

        $dimensions = new Dimensions();
        $dimensions->height = 1;
        $dimensions->width = 1;
        $dimensions->length = 1;
        $package->dimensions = $dimensions;

        $total = new Value();
        $total->amount = $deliveryData['value'];
        $total->currency = $deliveryData['currency'];
        $package->value = $total;

        $package->contents = $deliveryData['description'];

        if (isset($deliveryData['customs'])) {
            $customs = new PackageCustomsDeclaration();
            $customs->weight = $deliveryData['weight'];
            $customs->value = $total;
            $customs->contentsDescription = $deliveryData['description'];
            $customs->countryOfOrigin = $deliveryData['customs']['countryOfOrigin'];

            $package->customsDeclaration = $customs;

            $declaration = new CustomsDeclarationInfo();
            $declaration->categoryOfItem = $deliveryData['customs']['category'];
            $declaration->categoryOfItemExplanation = $deliveryData['customs']['explanation'];
            $declaration->termsOfTrade = $deliveryData['customs']['termsOfTrade'];
            $this->customsDeclarationInfo = $declaration;
        }

        $this->deliveryAddress = $deliveryAddress;
        $this->collectionAddress = $senderAddress;
        $this->contentsDescription = $deliveryData['description'];
        $this->reference1 = $deliveryData['reference'];
        $this->packages[] = $package;
    }
}