<?php

namespace Minioak\Whistl\Models\Shipment;

class CustomsDeclarationInfo
{
    public $termsOfTrade;

    public $postalCharges;

    public $categoryOfItem;

    public $categoryOfItemExplanation;

}