<?php

namespace Minioak\Whistl\Models\Shipment;

class Address
{
    public $contactName;

    public $companyName;

    public $email;

    public $phone;

    public $address1;

    public $address2;

    public $address3;

    public $city;

    public $area;

    public $postcode;

    public $country;

    public $addressType;
}