<?php

namespace Minioak\Whistl\Models\Shipment;

class CollectionDetails
{
    public $collectionDate;

    public $collectionReadyTime;

    public $locationCloseTime;
}