<?php

namespace Minioak\Whistl\Models\Shipment;

class PackageCustomsDeclaration
{
    public $contentsDescription;

    public $weight;

    public $value;

    public $hsTariffNumber;

    public $countryOfOrigin;
}