<?php

namespace Minioak\Whistl\Models\Shipment;

class ItemLevelDeclaration
{
    public $type;

    public $sku;

    public $description;

    public $quantity;

    public $value;

    public $currencyCode;

    public $countryOfOrigin;

    public $harmonisedCode;
}