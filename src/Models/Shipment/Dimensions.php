<?php

namespace Minioak\Whistl\Models\Shipment;

class Dimensions
{
    public $length;

    public $width;

    public $height;
}