<?php

namespace Minioak\Whistl\Models\Shipment;

class Package
{
    public $type;

    public $dimensions;

    public $weight;

    public $value;

    public $contents;

    public $customsDeclaration;

    public $itemLevelDeclarations;

    public $packageShippingInfo;
}