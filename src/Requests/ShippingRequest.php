<?php

namespace Minioak\Whistl\Requests;

use JMS\Serializer\SerializerBuilder;

class ShippingRequest extends Request
{
    protected $requestType = 'application/xml';

    protected $responseType = 'application/xml';

    protected $url = 'Shipment';

    protected $method = 'POST';

    protected $serialized = 'Minioak\Whistl\Models\Shipment\Shipment';

    public function  execute($parameters = false, $body = false)
    {
        $this->url = 'Shipment?' . http_build_query($parameters);
        return parent::execute(false, $body);
    }
}
