<?php

namespace Minioak\Whistl\Requests;

use JMS\Serializer\SerializerBuilder;

class ServiceRequest extends Request
{
    protected $requestType = 'application/xml';

    protected $responseType = 'application/xml';

    protected $url = 'Service';

    protected $method = 'POST';

    protected $serialized = 'Minioak\Whistl\Models\ServiceList';
}
