<?php

namespace Minioak\Whistl\Requests;

use JMS\Serializer\SerializerBuilder;
use Minioak\Whistl\Models\Shipment\Shipment;

class CancelShippingRequest extends Request
{
    protected $requestType = 'application/xml';

    protected $responseType = 'application/xml';

    protected $url = 'Shipment';

    protected $method = 'DELETE';

    public function execute($parameters = false, $body = false)
    {
        $this->url = "Shipment/${parameters}";
        return parent::execute(false, false);
    }

    public function parse($response)
    {
        return $response->getStatusCode() == 204;
    }
}
