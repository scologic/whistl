<?php

namespace Minioak\Whistl\Requests;

use GuzzleHttp\Client;

use GuzzleHttp\Exception\ServerException;
use JMS\Serializer\SerializerBuilder;
use Minioak\Whistl\Config\Config;
use Minioak\Whistl\Exceptions\WhistlException;

abstract class Request
{
    protected $requestType = false;

    protected $responseType = false;

    protected $url = false;

    protected $method = 'GET';

    protected $config = false;

    protected $client = false;

    protected $token = false;

    protected $serializer = false;

    protected $serialized = false;

    public function __construct(Config $config)
    {
        $this->config = $config->getConfig();
        $clientConfig = [
            'base_uri' => $config->getBaseUrl()
        ];

        if ($this->config['debug']) {
            $clientConfig['verify'] = false;
            $clientConfig['proxy'] = $this->config['proxy'];
        }

        $this->client = new Client($clientConfig);
        $this->token = $this->token();
        $this->serializer = SerializerBuilder::create()
            ->addMetadataDir(__DIR__ . '/../../resources/models')
            ->build();
    }

    public function getResponseType()
    {
        return $this->responseType;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getConfig()
    {
        return $this->config;
    }

    public function execute($parameters = false, $body = false)
    {
        $options = [];

        if (false !== $parameters) {
            $options['form_params'] = $parameters;
        }

        if (false !== $body) {
            $options['body'] = $body;
        }

        $options['headers'] = [
            'Content-Type' => $this->requestType,
            'Accept' => $this->getResponseType(),
            'User-Agent' => $this->config['user-agent'],
            'Username' => $this->config['account'],
            'Authorization' => 'Bearer '.$this->token
        ];

        try {
            $response = $this->client->request($this->method, $this->getUrl(), $options);

            return $this->parse($response);
        } catch (ServerException $ex) {
            $this->handleError($ex->getResponse());
        }
    }

    protected function handleError($response) {
        $serializer = SerializerBuilder::create()
            ->addMetadataDir(__DIR__ . '/../../resources/models')
            ->build();

        $result = $serializer->deserialize($response->getBody()->getContents(), 'Minioak\Whistl\Models\Error', 'xml');

        throw new WhistlException($result->message);
    }

    protected function token()
    {
        $response = $this->client->request('POST', 'token', [
            'form_params' => [
                'username' => $this->config['username'],
                'password' => $this->config['password'],
                'grant_type' => 'password'
            ]
        ]);

        if ($response->getStatusCode() == 200) {
            $body = json_decode($response->getBody()->getContents());
            return $body->access_token;
        }

        return false;
    }

    protected function parse($response)
    {
        return (false !== $this->serialized) ?
            $this->serializer->deserialize($response->getBody()->getContents(), $this->serialized, 'xml')
            : $response;
    }
}
