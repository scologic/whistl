<?php

namespace Minioak\Whistl\Requests;

use JMS\Serializer\SerializerBuilder;

class ServicePreferenceListRequest extends Request
{
    protected $requestType = 'application/x-www-form-urlencoded';

    protected $responseType = 'application/xml';

    protected $url = 'ServicePreferenceList';

    protected $method = 'GET';

    protected $serialized = 'Minioak\Whistl\Models\ServicePreferenceList';

}
