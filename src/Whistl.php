<?php

namespace Minioak\Whistl;

use Doctrine\Common\Annotations\AnnotationRegistry;
use JMS\Serializer\SerializerBuilder;
use Minioak\Whistl\Config\Config;
use Minioak\Whistl\Models\Shipment\Shipment;
use Minioak\Whistl\Requests\CancelShippingRequest;
use Minioak\Whistl\Requests\ServicePreferenceListRequest;
use Minioak\Whistl\Requests\ServiceRequest;
use Minioak\Whistl\Requests\ShippingRequest;

final class Whistl {

	protected $config;

	protected $serializer;

	public function __construct(Config $config)
	{
		$this->config = $config;
		$this->serializer = SerializerBuilder::create()
            ->addMetadataDir(__DIR__ . '/../resources/models')
            ->build();
	}
	
	public function serviceList()
	{
		$preferenceList = new ServicePreferenceListRequest($this->config);

		return $preferenceList->execute([
		    'AccountId' => $preferenceList->getConfig()['account']
        ]);
	}

    public function getService(Shipment $shipment)
    {
        $service = new ServiceRequest($this->config);

        return $service->execute(false, $this->serializer->serialize($shipment, 'xml'));
    }

    public function ship(Shipment $shipment)
    {
        $service = new ShippingRequest($this->config);

        return $service->execute([
            'RequestedLabelSize' => 8,
            'RequestedLabelFormat' => 'PNG'
        ], $this->serializer->serialize($shipment, 'xml'));
    }

    public function cancel($shipment)
    {
        $service = new CancelShippingRequest($this->config);

        return $service->execute($shipment, false);
    }

}