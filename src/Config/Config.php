<?php 

namespace Minioak\Whistl\Config;

final class Config
{
    const SANDBOX_URL = 'https://api.whistl.parcelhub.net/';
    const PRODUCTION_URL = 'https://api.parcelhub.net/1.0/';

    protected $config = [
        'sandbox' => true,
        'username' => '',
        'password' => '',
        'account' => '',
        'debug' => false,
        'user-agent' => 'Whistl Php Library 1.0'
    ];

    public function __construct($config)
    {
        $this->config = array_merge($this->config, $config);
    }

    public function getBaseUrl()
    {
        return $this->config['sandbox'] ? self::SANDBOX_URL : self::PRODUCTION_URL;
    }

    public function getConfig()
    {
        return $this->config;
    }
}
