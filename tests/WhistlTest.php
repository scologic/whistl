<?php

use JMS\Serializer\SerializerBuilder;
use PHPUnit\Framework\TestCase;

use Minioak\Whistl\Whistl;
use Minioak\Whistl\Config\Config;

final class WhistlTest extends TestCase
{
    protected $service;

    protected $serializer;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $config = new Config([
            'sandbox' => true,
            'username' => getenv('WHISTL_USERNAME'),
            'password' => getenv('WHISTL_PASSWORD'),
            'account' => getenv('WHISTL_ACCOUNT')
        ]);

        $this->service = new Whistl($config);

        $this->serializer = SerializerBuilder::create()
            ->addMetadataDir(__DIR__ . '/../resources/models')
            ->build();
    }

    public function testFetchServicePreferenceList()
    {
        $response = $this->service->serviceList();

        $this->assertEquals($response->servicePreferences[0]->id, 471);
        $this->assertEquals($response->servicePreferences[0]->preferences[0]->id, 77777);
    }

    public function testDeserializeShipmentMessage()
    {
        $sample = file_get_contents(__DIR__ . '/../resources/sample/shipment.xml');

        $result = $this->serializer->serialize($this->serializer->deserialize($sample, 'Minioak\Whistl\Models\Shipment\Shipment', 'xml'), 'xml');

        $this->assertEquals($this->formatXml($sample), $this->formatXml($result));
    }

    public function testGetServices()
    {
        $sample = file_get_contents(__DIR__ . '/../resources/sample/shipment.xml');

        $shipment = $this->serializer->deserialize($sample, 'Minioak\Whistl\Models\Shipment\Shipment', 'xml');

        $result = $this->service->getService($shipment);

        $this->assertNotNull($result);

    }

    public function testShipment()
    {
        $sample = file_get_contents(__DIR__ . '/../resources/sample/basic-shipping.xml');

        $shipment = $this->serializer->deserialize($sample, 'Minioak\Whistl\Models\Shipment\Shipment', 'xml');

        $services = $this->service->getService($shipment);

        $shipment->serviceInfo = $services->suitable[0]->preference;

        $result = $this->service->ship($shipment);

        $this->assertNotNull($result->packages[0]->packageShippingInfo->labels[0]->image);
        $this->assertNotNull($result->shippingInfo->trackingNumber);
        $this->assertNotNull($result->id);

        return $result;
    }

    public function testCancelShipment()
    {
        $shipment = $this->testShipment();

        $cancel = $this->service->cancel($shipment->id);

        $this->assertTrue($cancel);
    }

    public function testInternationalShipment()
    {
        $sample = file_get_contents(__DIR__ . '/../resources/sample/international-shipping.xml');

        $shipment = $this->serializer->deserialize($sample, 'Minioak\Whistl\Models\Shipment\Shipment', 'xml');

        $services = $this->service->getService($shipment);

        $shipment->serviceInfo = $services->suitable[0]->preference;

        $result = $this->service->ship($shipment);

        $this->assertNotNull($result->packages[0]->packageShippingInfo->labels[0]->image);
        $this->assertNotNull($result->shippingInfo->trackingNumber);
        $this->assertNotNull($result->id);

        return $result;
    }

    public function testInvalidAccountId()
    {
        $this->expectException(\Minioak\Whistl\Exceptions\WhistlException::class);

        $sample = file_get_contents(__DIR__ . '/../resources/sample/invalid-shipment.xml');

        $shipment = $this->serializer->deserialize($sample, 'Minioak\Whistl\Models\Shipment\Shipment', 'xml');

        $result = $this->service->getService($shipment);
    }

    private function formatXml($data)
    {
        $xmlDocument = new DOMDocument('1.0');
        $xmlDocument->preserveWhiteSpace = false;
        $xmlDocument->formatOutput = true;
        $xmlDocument->loadXML($data);

        return $xmlDocument->saveXML();
    }
}
